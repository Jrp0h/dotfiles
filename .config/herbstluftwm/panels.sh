killall polybar

primary_monitor=$(xrandr -q | grep "primary" | awk '{ print $1; }')

MONITOR=$primary_monitor polybar -r example &
sleep 1

for m in $(xrandr --query | grep " connected" | tac | cut -d" " -f1); do
    if [ "$m" != "$primary_monitor" ]; then
        MONITOR=$m polybar -r example &
    fi
done
