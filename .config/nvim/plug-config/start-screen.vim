let g:startify_session_dir = '~/.config/nvim/session'
let g:startify_fortune_use_unicode = 1

let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
          \ { 'type': 'sessions',  'header': ['   Sessions']       },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ ]

let g:startify_bookmarks = [
            \ { 'b': '~/.config/bspwm/bspwmrc' },
            \ { 'h': '~/.config/herbstluftwm/autostart' },
            \ { 's': '~/.config/sxhkd/sxhkdrc' },
            \ { 'i': '~/.config/nvim/init.vim' },
            \ { 'p': '~/.config/polybar/config' },
            \ { 'z': '~/.zshrc' },
            \ ]
