;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Marcus Nilsson"
      user-mail-address "marcus.nilsson@genarp.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "JetBrains Mono" :size 13 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 14))

(setq doom-font (font-spec :family "JetBrains Mono" :size 13 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/dev/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(add-hook 'before-save-hook #'+format/buffer nil t)

;; (remove-hook 'before-save-hook #'+format/buffer nil t)

(setq x-super-keysym 'meta)
(setq projectile-project-search-path '(("~/Documents/dev" . 2)))

(setq tab-width 4)

(map! "C-'" #'comment-line)

(remove-hook 'doom-first-input-hook #'evil-snipe-mode)

;; (after! org
;;   (setq org-todo-keywords '((sequence "TODO(t)" "INPROGRESS(i)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)"))
;;         org-todo-keyword-faces
;;         '(("TODO" :foreground "#FE8019" :weight normal :underline t)
;;           ("WAITING" :forground "fadb2f" :weight normal :underline t)
;;           ("INPROGRESS" :foreground "#458588" :weight normal :underline t)
;;           ("DONE" :foreground "#B8BB26" :weight bold :underline t)
;;           ("CANCELLED" :foreground "#FB4934" :weight normal :underline t))))
;; 
;; (custom-set-variables
;;  '(package-selected-packages '(fzf js-react-redux-yasnippets rjsx-mode tide)))
;; 
;; (after! rustic
;;   (setq rustic-lsp-server 'rust-analyzer))
;; 
;; (use-package lsp-mode
;;   :hook ((c++-mode) . lsp-deferred)
;;   :commands lsp
;;   :config
;;   (setq lsp-clients-clangd-args '("-j=4" "-background-index" "--log=error" "--clang-tidy" "--enable-config"))
;;   (setq lsp-clangd-binary-path "/usr/bin/clangd"))

(set 'format-all-show-errors 'always)
;; (setq-hook! 'c++-mode-hook +format-with 'clang-format)

;; (lsp-register-client
;;  (make-lsp-client
;;   :new-connection (lsp-stdio-connection ("blueprint-compiler" "lsp"))
;;   :major-modes '(blueprint-mode)
;;   :server-id 'blueprint-compiler))


(require 'lsp-mode)
(lsp-register-client
 (make-lsp-client :new-connection (lsp-stdio-connection '("blueprint-compiler" "lsp"))
                  :major-modes '(blueprint-mode)
                  :priority -1
                  :server-id 'blueprint-compiler))
;; (lsp-register-client
;;  (make-lsp-client :new-connection (lsp-stdio-connection '("gleam" "lsp"))
;;                   :major-modes '(gleam-mode)
;;                   :priority -1
;;                   :server-id 'gleam-compiler))

(add-hook 'blueprint-mode-hook #'lsp!)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.razor?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.cshtml?\\'" . web-mode))

(setq web-mode-engines-alist
      '(("razor" . "\\.razor\\'")
        ("razpr" . "\\.cshtml\\'")))

(after! lsp-ui
	(setq lsp-ui-sideline-diagnostic-max-lines 10))
