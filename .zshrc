export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="dpoggi"

plugins=(git zsh-syntax-highlighting zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh

# Enviorment variables
export EDITOR="emacsclient -t -a ''"
export VISUAL="emacsclient -c -a emacs" 
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export GOPATH="$HOME/Documents/dev/go"

# Quality of life
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias more=less
alias sdn="doas shutdown -P now"          # sdn = "sudo shutdown now"
alias du="gdu"
alias ls="exa --color=always --git"

# Git
alias ga="git add"
alias gpull="git pull"
alias gc="git commit -m"
alias gpush="git push"

# Python
alias python="python3"
alias pip="pip3"

# Laravel
alias artisan="php artisan"
alias tinker="php artisan tinker"
alias migrate="php artisan migrate"

# AdonisJS
alias ace="node ace"
alias arepl="node ace repl"

# DOAS IS BETTER THAN SUDO!!!! >:(
alias doas="doas --"
alias sudo="doas"

# Neovim > vim
alias vim="nvim"
alias v="nvim"

# I'm too used to using :q to quit stuff
alias :q="exit"

# Misc
alias mon2cam="deno run --unstable -A -r -q https://raw.githubusercontent.com/ShayBox/Mon2Cam/master/src/mod.ts"  # Program used for sending my monitors buffer in to a fake webcam so
                                                                                                                  # I can stream my monitor to friends over discord without showing all
                                                                                                                  # of my monitors at the same time making it impossible for them to see
                                                                                                                  # what I'm trying to show them.

# Extract different archives
# Shamelessly stolen from either Manjaro i3 edition default .bashrc or Ubuntu 20.04 LTS default .bashrc
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

## I'm-too-lazy-to-write-all-that functions
ds() {
  $HOME/.emacs.d/bin/doom sync
}

sx() {
  startx ~/.config/X11/xinitrc
}

# Paths
export PATH="$PATH:$HOME/Android/Sdk/emulator"
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$HOME/opt/cross/bin"
export PATH="$PATH:/var/lib/snapd/snap/bin"
export PATH="$PATH:$HOME/.emacs.d/bin"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:/usr/lib/node_modules/npm"
export PATH="$PATH:$HOME/Documents/dev/go/bin"

export CMAKE_GENERATOR=Ninja
