#!/bin/bash

function config_deps() {
    sudo pacman -S --noconfirm --needed base-devel
    sudo pacman -S --noconfirm git make xorg arandr xdg-user-dirs curl wget vim neovim man-db man-pages dash fish
}

function config_doas() {
    paru -S doas --noconfirm

    cat <<EOF > /tmp/doas.conf
permit persist :wheel as root
permit nopass :wheel as root cmd reflector
EOF

    sudo mv /tmp/doas.conf /etc/doas.conf
    sudo chmod 644 /etc/doas.conf
    sudo chown root:root /etc/doas.conf
}

function config_deps_from_paru() {
    paru -S gruvbox-material-icon-theme gruvbox-material-gtk-theme-git --noconfirm
}

function config_paru() {
    git clone https://aur.archlinux.org/paru.git
    cd paru
    makepkg -si --noconfirm
    cd ..
    rm -rf paru
}

function config_bash() {
    mv $HOME/.bashrc $HOME/.bashrc.bak
    ln -s $(pwd)/.bashrc $HOME/.bashrc
}

function config_fish() {
    mv $HOME/.config/fish $HOME/.config/fish.bak
    ln -s $(pwd)/.config/fish $HOME/.config/fish
}

# Herbstluftwm
function config_herbstluftwm() {
    sudo pacman -S herbstluftwm --noconfirm

    mkdir -p $HOME/.config/herbstluftwm

    herbstluft_files="$(ls -la .config/herbstluftwm | tail -n +4 | awk '{ print $9; }')"
    for f in $herbstluft_files
    do
        ln -s $(pwd)/.config/herbstluftwm/$f $HOME/.config/herbstluftwm/$f
    done
}


# Doom Emacs
function config_doom_emacs () {
    sudo pacman -S emacs --noconfirm

    git clone --depth 1 https://github.com/hlissner/doom-emacs $HOME/.emacs.d
    $HOME/.emacs.d/bin/doom install -y

    rm -rf $HOME/.doom.d
    mkdir -p $HOME/.doom.d

    doom_file="$(ls -la .doom.d | tail -n +4 | awk '{ print $9; }')"
    for f in $doom_file
    do
        ln -s $(pwd)/.doom.d/$f $HOME/.doom.d/$f
    done

    $HOME/.emacs.d/bin/doom sync
}

# Alacritty
function config_alacritty() {
    sudo pacman -S alacritty --noconfirm

    mkdir -p $HOME/.config/alacritty
    ln -s $(pwd)/.config/alacritty/alacritty.yml $HOME/.config/alacritty/alacritty.yml
}

function config_lightdm() {
    sudo pacman -S lightdm lightdm-gtk-greeter --noconfirm
    sudo systemctl enable lightdm
}

# picom
function config_picom() {
    sudo pacman -S picom --noconfirm

    mkdir -p $HOME/.config/picom
    ln -s $(pwd)/.config/picom/picom.conf $HOME/.config/picom/picom.conf
}

# Polybar
function config_polybar() {
    paru -S polybar --noconfirm

    mkdir -p $HOME/.config/polybar
    ln -s $(pwd)/.config/polybar/config $HOME/.config/polybar/config
}

# xob
function config_xob() {
    paru -S xob --noconfirm

    mkdir -p $HOME/.config/xob
    ln -s $(pwd)/.config/xob/styles.cfg $HOME/.config/xob/styles.cfg
}

# dunst
function config_dunst() {
    paru -S dunst --noconfirm

    mkdir -p $HOME/.config/dunst
    ln -s $(pwd)/.config/dunst/dunstrc $HOME/.config/dunst/dunstrc
}

# dmenu
function config_dmenu() {
    git clone https://gitlab.com/Jrp0h/my-dmenu
    cd my-dmenu
    make clean
    make
    sudo make install
    cd ..
    rm -rf my-dmenu
}

function config_scripts() {
    git clone https://gitlab.com/Jrp0h/scripts $HOME/scripts
}

function config_zsh() {
    sudo pacman -S --noconfirm zsh zsh-completions
    sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    mkdir -p ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    git clone https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    rm $HOME/.zshrc
    ln -s $(pwd)/.zshrc $HOME/.zshrc
}

function config_profile_file() {
    mv $HOME/.profile $HOME/.profile.bak
    echo "ENV=$(pwd)/initsh; export ENV" > $HOME/.profile
}

function start() {
    config_deps
    config_paru

    config_scripts
    config_lightdm
    config_herbstluftwm
    config_doom_emacs
    config_alacritty
    config_picom
    config_polybar
    config_xob
    config_dunst

    config_bash

    config_deps_from_paru
    config_dmenu
    config_fish

    config_profile_file

    finish
}

function finish() {
    paru -S brave-bin --noconfirm
    sudo pacman -S network-manager-applet picom flameshot nitrogen reflector ccls gdu --noconfirm

    config_doas

    sudo pacman -S openssh --noconfirm
    sudo pacman -S --noconfirm ttf-jetbrains-mono ttf-font-awesome

    sudo pacman -S --noconfirm lxappearance bat ripgrep exa

    git clone https://gitlab.com/Jrp0h/Wallpapers.git $HOME/Pictures/Wallpapers/

    clear
    echo "Installation DONE!"
    echo "Please install and run arandr to configure monitor layout and save the layout to $HOME/.screenlayouts/herbstluft.sh"
}

start

# TODO:
#   Configure nvim?
#   Configure betterlockscreen
